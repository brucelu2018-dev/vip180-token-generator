# issue token

## 一键发币
```
npm install
```

### Compiles and hot-reloads for development
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
###Detail Step
```
本地开发: 1、npm install; 2、npm run dev

线上部署: 1、npm install; 2、npm run build; 3、将dist目录下build好的文件放到服务器上即可

tip: 以上命令均在项目目录下的终端执行
